<?php
/**
 * Template Name: Home Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>

<section id="body">
    
    <div class="hideme">
        <?php
        $focus_image = wp_get_attachment_image_src(get_field('focus_image'), 'full');
        $history_image = wp_get_attachment_image_src(get_field('history_image'), 'full');
        $mission_image = wp_get_attachment_image_src(get_field('mission_image'), 'full');
        $objactive_image = wp_get_attachment_image_src(get_field('objactive_image'), 'full');
        $focus_image1 = $focus_image[0];
        $history_image1 = $history_image[0];
        $mission_image1 = $mission_image[0];
        $objective_image1 = $objactive_image[0]; 
        ?>
        <img src="<?php echo $focus_image[0]; ?>" alt="#">
        <img src="<?php echo $history_image[0]; ?>" alt="#">
        <img src="<?php echo $mission_image[0]; ?>" alt="#">
        <img src="<?php echo $objactive_image[0]; ?>" alt="#">

    </div>
    <div class="row background-row">
        <div class="home-header-bar header-bar" id="homeheaderbar">
            <div class="home_left_col float_left">
                <div class="tab">
                    <button class="tablinks focus_tablinks"  onclick="openCity(event, 'focus')"><?php the_field('focus_title'); ?></button>
                    <button class="tablinks history_tablinks"  onclick="openCity(event, 'history')"><?php the_field('history_title'); ?></button>
                    <button class="tablinks mission_tablinks"  onclick="openCity(event, 'mission')"><?php the_field('mission_title'); ?></button>
                    <button class="tablinks objective_tablinks" onclick="openCity(event, 'objective')"><?php the_field('objactive_title'); ?></button>
                </div>
            </div>
            <div class="home_right_col float_left">
                <div class="caption">
                    <div class="caption-holder">
                        <div id="focus" class="tabcontent">
                            <p><?php the_field('focus_detail'); ?></p>
                        </div>
                        <div id="history" class="tabcontent">
                            <p><?php the_field('history_detail'); ?> </p>
                        </div>
                        <div id="mission" class="tabcontent">
                            <p><?php the_field('mission_detail'); ?> </p>
                        </div>
                        <div id="objective" class="tabcontent">
                            <p><?php the_field('objactive_detail'); ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<script>
 function openCity(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
        $(".tablinks").css("text-align","left");
    }
    $('.tabcontent').hide();

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(cityName).style.display = "block";
    //evt.currentTarget.className += " active";
    $("."+cityName+"_tablinks").addClass('active');
    //alert (cityName);
    //var urlString = 'url("http://localhost/firstsustainable/wp-content/themes/firstsustainable/assets/images/'+ cityName +'_image")';
    if(cityName == "focus"){
        //alert(yes);
        var urlString = 'url(<?php echo $focus_image1; ?>)';
        //alert(cityName);
    }else if (cityName == "history") {
        //alert(cityName);
        var urlString = 'url(<?php echo $history_image1; ?>)';
    }else if (cityName == "mission") {
        //alert(cityName);
        var urlString = 'url(<?php echo $mission_image1; ?>)';
    }else if (cityName == "objective") {
        //alert(cityName);
        var urlString = 'url(<?php echo $objective_image1; ?>)';
    }
        document.getElementById("homeheaderbar").style.backgroundImage =  urlString;    
    $(".tablinks.active").css("text-align","right");
}

</script>
<?php
get_footer();
