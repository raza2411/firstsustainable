<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>First Sustainable</title>

        <!-- Bootstrap -->
        <link href="<?php bloginfo('template_directory')?>/assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php bloginfo('template_directory')?>/assets/css/style.css" rel="stylesheet">

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory')?>/assets/js/custom.js"></script>
    </head>

    <body <?php body_class(); ?>>

 <?php
        if (is_front_page() || is_home()) {
           
        } else {
            
        }
        ?>

        <!-- Global site tag (gtag.js) - Google Analytics -->

        <div class="container-fluid ">
            <header>
                <div class="row"> 

                    <nav class="navbar navbar-default navbar-static-top  nav-pills navbar-fixed-top">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-right">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>                        
                            </button>
                            <a href="<?php bloginfo('url'); ?>"> <img src="<?php echo get_option_tree('logo_image', '', false); ?>"></a>
                            <a class="logo_tagline" href="http://777part.com/"> <img src="<?php echo get_option_tree('logo_tagline', '', false); ?>"></a>
                        </div>
                        
                            <?php
                        wp_nav_menu(
                                array(
                                    'menu' => 'Main Menu',
                                    'container' => '',
                                    'container_class' => '',
                                    'container_id' => '',
                                    'menu_class' => 'nav navbar-nav collapse navbar-collapse navbar-right',
                                    'menu_id' => '',
                                )
                        );
                        ?>
                       
                    </nav>
                </div>
            </header> 