<?php
/**
 * Template Name: Business Line Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>

<div class="row background-row">
    <div class="sidebar businesssidebar" style="background-image:url('<?php the_post_thumbnail_url(); ?>');">
        <div class="caption">
            <div class="caption_holder">
                <h1><?php the_title(); ?></h1>
            </div>
        </div>

    </div>
    <div class="content_bar">
        <?php
        if (have_rows('team-content')):

            while (have_rows('team-content')) : the_row();
                $name = get_sub_field('name');
                $designation = get_sub_field('designation');
                $caption = get_sub_field('caption');
                $image = wp_get_attachment_image_src(get_sub_field('banner-image'), 'full');
                ?>
                <section id="business1_sec" class="content_section" >
                    <div class="business business1 col-md-12" style="background-image:url('<?php echo $image[0]; ?>');">
                        <div class="business_header business_text col-md-5">
                            <div class="caption">
                                <div class="caption-holder"> 
                                    <h1><?php echo $name; ?></h1>
                                </div>
                            </div>
                        </div>
                        <div class="business_text  col-md-7">
                            <div class="caption">
                                <div class="caption-holder"> 
                                    <p><?php echo $caption; ?> </p>


                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div> 
                </section>  
                <?php
            endwhile;
        endif;
        ?> 
    </div>
</div>
<?php
get_footer();
