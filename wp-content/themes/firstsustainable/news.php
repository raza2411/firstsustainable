<?php
/**
 * Template Name: News Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>

 <div class="row background-row">
       <div class="header-bar new-full-height">
        <div class="sidebar newssidebar" style="background-image:url('<?php the_post_thumbnail_url(); ?>');">
          <div class="caption">
              <div clas<?php the_title; ?></h1>
              </div>
          </div>
           
        </div>
            <?php
        if (have_rows('team-content')):

            while (have_rows('team-content')) : the_row();
                $name = get_sub_field('name');
                $image = wp_get_attachment_image_src(get_sub_field('banner-image'), 'full');
                ?>
        <div class="news-header-bar" style="background-image:url('<?php echo $image[0]; ?>');">
          <div class="caption">
            <div class="caption-holder">
               <span id="cont-mid-heading">  
               <h1><?php echo $name; ?></h1>
                    
               </span>
            </div>
          </div>
        </div>
           <?php
            endwhile;
        endif;
        ?>
        <div class="clearfix"></div>
       </div>
      </div>
<?php
get_footer();
