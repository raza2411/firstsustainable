<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */
?>

<footer id="footer">
    <div class="row">
        <div class="panel-footer">
            <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12"> <span></span></div>
            <div class="col-sm-4 col-md-4 col-lg-4 center_footer col-xs-12"> <span id="center_footer">A Portfolio Company of <a href="<?php echo get_option_tree('company_portfolio', false); ?>" target="_blank"> 777 Partners</a></span></div>
            <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12"> <span id="right-footer">site by&nbsp;<a href="<?php echo get_option_tree('power_by', false); ?>" target="_blank">MFVS</a></span></div>
        </div>
    </div>
</footer>
</div>




 <script src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>
